use std::sync::{mpsc::Sender, Arc};

use waef_controls::system::window::OnWindowCloseRequested;
use waef_core::{
    actors::{ActorAlignment, HasActorAlignment, IsActor},
    core::{DispatchFilter, ExecutionResult, IsDispatcher, IsDisposable, IsMessage, Message},
    ecs::EcsRegistry,
};
use waef_wasmactor::{actor::dispatcher, ecs::WasmEcsRegistry, WasmActor};

struct GameCodeActor {
    dispatcher: Sender<Message>,
    ecs: Arc<dyn EcsRegistry>,
}
impl IsDispatcher for GameCodeActor {
    fn dispatch(&mut self, message: &Message) -> ExecutionResult {
        if message.name == OnWindowCloseRequested::category_name() {
            ExecutionResult::Kill
        } else if message.name == "hot_reload:mount" {
            ExecutionResult::NoOp
        } else if message.name == "hot_reload:unmount" {
            ExecutionResult::NoOp
        } else {
            ExecutionResult::NoOp
        }
    }

    fn filter(&self) -> DispatchFilter {
        DispatchFilter::OneOf([
            OnWindowCloseRequested::category_name().into(),
            "hot_reload:mount".into(),
            "hot_reload:unmount".into()
        ].into())
    }
}
impl HasActorAlignment for GameCodeActor {
    fn alignment(&self) -> ActorAlignment {
        ActorAlignment::Processing
    }
}
impl IsDisposable for GameCodeActor {
    fn dispose(&mut self) {}
}
impl IsActor for GameCodeActor {
    fn weight(&self) -> u32 {
        1
    }

    fn name(&self) -> String {
        "game_code".to_string()
    }
}

pub fn main() {
    let dispatcher: Sender<Message> = dispatcher();
    let ecs = WasmEcsRegistry::create();

    WasmActor::new()
        .use_actor(GameCodeActor { ecs, dispatcher })
        .start();
}
