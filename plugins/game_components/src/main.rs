use game_components::TestComponent;
use waef_wasmactor::ecs::WasmEcsRegistry;
use waef_core::EcsExtension;

pub fn main() {
    let ecs = WasmEcsRegistry::create();

    _ = ecs.define_component::<TestComponent>();
}
