/* tslint:disable */
/* eslint-disable */
export const memory: WebAssembly.Memory;
export function main(a: number, b: number): number;
export function canonical_abi_realloc(a: number, b: number, c: number, d: number): number;
export function __wbg_trap_free(a: number): void;
export function trap___wbg_wasmer_trap(): void;
export function wgpu_render_bundle_set_pipeline(a: number, b: number): void;
export function wgpu_render_bundle_set_bind_group(a: number, b: number, c: number, d: number, e: number): void;
export function wgpu_render_bundle_set_push_constants(a: number, b: number, c: number, d: number, e: number): void;
export function wgpu_render_bundle_draw_indirect(a: number, b: number, c: number): void;
export function wgpu_render_bundle_draw_indexed_indirect(a: number, b: number, c: number): void;
export function wgpu_render_bundle_set_vertex_buffer(a: number, b: number, c: number, d: number, e: number): void;
export function wgpu_render_bundle_set_index_buffer(a: number, b: number, c: number, d: number, e: number): void;
export function wgpu_render_bundle_draw(a: number, b: number, c: number, d: number, e: number): void;
export function wgpu_render_bundle_draw_indexed(a: number, b: number, c: number, d: number, e: number, f: number): void;
export function wgpu_render_bundle_pop_debug_group(a: number): void;
export function wgpu_render_bundle_insert_debug_marker(a: number, b: number): void;
export function wgpu_render_bundle_push_debug_group(a: number, b: number): void;
export function canonical_abi_free(a: number, b: number, c: number): void;
export function __wbindgen_export_0(a: number, b: number): number;
export function __wbindgen_export_1(a: number, b: number, c: number, d: number): number;
export const __wbindgen_export_2: WebAssembly.Table;
export function __wbindgen_export_3(a: number, b: number): void;
export function __wbindgen_export_4(a: number, b: number, c: number): void;
export function __wbindgen_export_5(a: number, b: number, c: number, d: number): void;
export function __wbindgen_export_6(a: number): void;
export function __wbindgen_start(): void;
